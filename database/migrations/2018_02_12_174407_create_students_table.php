<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');

            $table->string('studentName');
            $table->string('email');
            $table->string('address');
            $table->string('nameOfInstitution');
            $table->string('subject');
            $table->string('passingYear');
            $table->string('dateOfBirth');
            $table->string('religion');
            $table->string('fatherName');
            $table->string('fatherOccupation');
            $table->string('fatherContactNo');
            $table->string('fatherNIDno');
            $table->string('motherName');
            $table->string('motherOccupation');
            $table->string('motherContactNo');
            $table->string('motherNIDno');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
