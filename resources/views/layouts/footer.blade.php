<div class="col-md-12 rowFooter navbar navbar-fixed-bottom">
				<div class="col-md-14"></div>
					&copy; {{ @date('Y') }} - All rights reserved by 
						<a href="https://www.facebook.com/hariz.mohammad.7" target="_new">
							S Ahmed Naim
						</a>
				</div>
			</div>
		</div>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>