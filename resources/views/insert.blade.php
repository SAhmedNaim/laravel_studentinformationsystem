@extends('layouts.app')

@section('content')

<div class="col-md-1"></div>
	<div class="col-md-10">

		@if($errors->any())
			@foreach($errors->all() as $error)
				<div class="alert alert-danger alert-dismissable fade in">
		    		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		    		<strong>Warning!</strong> {{ $error }}
				</div>
			@endforeach
		@endif

		<form method="POST" action="{{ route('insertStudents') }}" enctype="multipart/form-data">

			{{ csrf_field() }}

			<table class="table table-hover table-striped table-responsive padded_table">
					
					<!-- Row Heading -->
					<tr>
						<td class="tableHeader">Student's Information</td>
						<td class="tableHeader">Parent's Information</td>
					</tr>
					
					<!-- 1st Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="studentName">Student's Name: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="studentName" />
	  						</div>
						</td>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="fatherName">Father's Name: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="fatherName" />
	  						</div>
						</td>
					</tr>
					
					<!-- 2nd Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="email">E-mail ID: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="email" name="email" />
	  						</div>
						</td>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="fatherOccupation">
									Father's Occupation: 
								</label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="fatherOccupation" />
	  						</div>
						</td>
					</tr>

					<!-- 3rd Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="address">Present Address: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="address" />
	  						</div>
						</td>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="fatherContactNo">
									Father's Contact No: 
								</label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="number" name="fatherContactNo" />
	  						</div>
						</td>
					</tr>
					
					<!-- 4th Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="nameOfInstitution">
									Name of Institution: 
								</label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="nameOfInstitution" />
	  						</div>
						</td>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="fatherNIDno">Father's NID No: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="number" name="fatherNIDno" />
	  						</div>
						</td>
					</tr>
					
					<!-- 5th Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="subject">Subject: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="subject" />
	  						</div>
						</td>

						<td class="col-md-5">
							<div class="col-md-5">
								<label for="motherName">Mother's Name: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="motherName" />
	  						</div>
						</td>
					</tr>
					
					<!-- 6th Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="passingYear">Passing Year: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="number" name="passingYear" />
	  						</div>
						</td>

						<td class="col-md-6">
							<div class="col-md-5">
								<label for="motherOccupation">Mother's Occupation: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="motherOccupation" />
	  						</div>
						</td>
					</tr>
					
					<!-- 7th Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="dateOfBirth">Date of Birth: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="date" name="dateOfBirth" />
	  						</div>
						</td>
														
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="motherContactNo">
									Mother's Contact No: 
								</label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="number" name="motherContactNo" />
	  						</div>
						</td>

					</tr>
					
					<!-- 8th Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="religion">Religion: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="religion" />
	  						</div>
						</td>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="motherNIDno">Mother's NID No: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="number" name="motherNIDno" />
	  						</div>
						</td>
					</tr>
					
			</table>
			<div class="submit padded_submit" >
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<input type="submit" name="submit" value="Save" class="btn btn-info btn-submit" />
				</div>
				<div class="col-md-4"></div>
			</div>
		</form>
	</div>
<div class="col-md-1"></div>

@endsection