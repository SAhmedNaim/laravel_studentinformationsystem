@extends('layouts.app')

@section('content')

<div class="container-fluid" style="margin-bottom:80px;">
    <div class="col-md-1"></div>
    <div class="col-md-10">        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2>
                    Show All Students
                
                    <span class="col-lg-4 pull-right">

                      <div class="col-lg-6">
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="Type Student Name..." style="width:250px !important;">
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Search</button>
                          </span>
                        </div><!-- /input-group -->
                      </div><!-- /.col-lg-6 -->

                    </span>
                </h2>
            </div>
            
            <div class="panel-body">

                @if(session('successMsg'))
                    <div class="alert alert-success alert-dismissable fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> {{ session('successMsg') }}
                    </div>
                @endif
                
                <table class="table table-hover table-striped table-responsive padded_table" style="text-align:center;">
                    <tr style="text-align:center; font-weight:bold;">
                        <td>Student ID</td>
                        <td>Student's Name</td>
                        <td>Student's E-mail</td>
                        <td>Action</td>
                    </tr>
                    
                    @php 
                    $i = $students->perPage() * ($students->currentPage()-1)
                    @endphp

                    @foreach($students as $student)
                    
                        <tr>
                            <td>{{ $i = $i + 1 }}</td>
                            <td>{{ $student->studentName }}</td>
                            <td>{{ $student->email }}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('insert') }}">
                                                <span class="glyphicon glyphicon-plus"> Insert</span>
                                            </a>
                                        </li>
                                        
                                        <li>
                                            <a href="{{ route('update', $student->id) }}">
                                                <span class="glyphicon glyphicon-pencil"> Update</span>
                                            </a>
                                        </li>

                                        <form method="POST" action="{{ route('delete', $student->id) }}" id="delete-form-{{ $student->id }}" style="display: none;">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                        </form>

                                        <li>
                                            <a href="{{ route('delete', $student->id) }}" onclick="if(confirm('Are you sure to delete?')){
                                                event.preventDefault();
                                                document.getElementById('delete-form-{{ $student->id }}').submit();
                                            } else {
                                                event.preventDefault();
                                            }"
                                            />
                                                <span class="glyphicon glyphicon-trash"> Delete</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                </table>

                <div class="col-md-4 col-lg-4"></div>
                <div class="col-md-4 col-lg-4">
                    <!-- Pagination -->
                    {{ $students->links() }}
                </div>
                <div class="col-md-4 col-lg-4"></div>

            </div>
        </div>

    </div>
    <div class="col-md-1"></div>
</div>

 @endsection