<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;

class StudentController extends Controller
{
    public function showStudents()
    {
    	$students = Students::paginate(3);
    	return view('home', compact('students'));
    }

    public function insert()
    {
    	return view('insert');
    }

    public function insertStudents(Request $request)
    {
    	$this->validate($request, [
    		'studentName'		=> 'required',
    		'fatherName'		=> 'required',
    		'email'				=> 'required',
    		'fatherOccupation'	=> 'required',
    		'address'			=> 'required',
    		'fatherContactNo'	=> 'required',
    		'nameOfInstitution'	=> 'required',
    		'fatherNIDno'		=> 'required',
    		'subject'			=> 'required',
    		'motherName'		=> 'required',
    		'passingYear'		=> 'required',
    		'motherOccupation'	=> 'required',
    		'dateOfBirth'		=> 'required',
    		'motherContactNo'	=> 'required',
    		'religion'			=> 'required',
    		'motherNIDno'		=> 'required'
    	]);

    	$students = new Students;
    	$students->studentName 			= $request->studentName;
    	$students->fatherName 			= $request->fatherName;
    	$students->email 				= $request->email;
    	$students->fatherOccupation 	= $request->fatherOccupation;
    	$students->address 				= $request->address;
    	$students->fatherContactNo		= $request->fatherContactNo;
    	$students->nameOfInstitution	= $request->nameOfInstitution;
    	$students->fatherNIDno 			= $request->fatherNIDno;
    	$students->subject 				= $request->subject;
    	$students->motherName 			= $request->motherName;
    	$students->passingYear			= $request->passingYear;
    	$students->motherOccupation 	= $request->motherOccupation;
    	$students->dateOfBirth 			= $request->dateOfBirth;
    	$students->motherContactNo 		= $request->motherContactNo;
    	$students->religion 			= $request->religion;
    	$students->motherNIDno 			= $request->motherNIDno;
    	$students->save();
    	return redirect(route('home'))->with('successMsg', 'Student Inserted Successfully!!!');
    }

    public function update($id)
    {
    	$students = Students::find($id);
    	return view('update', compact('students'));
    }

    public function updateStudents(Request $request, $id)
    {
    	$this->validate($request, [
    		'studentName'		=> 'required',
    		'fatherName'		=> 'required',
    		'email'				=> 'required',
    		'fatherOccupation'	=> 'required',
    		'address'			=> 'required',
    		'fatherContactNo'	=> 'required',
    		'nameOfInstitution'	=> 'required',
    		'fatherNIDno'		=> 'required',
    		'subject'			=> 'required',
    		'motherName'		=> 'required',
    		'passingYear'		=> 'required',
    		'motherOccupation'	=> 'required',
    		'dateOfBirth'		=> 'required',
    		'motherContactNo'	=> 'required',
    		'religion'			=> 'required',
    		'motherNIDno'		=> 'required'
    	]);

    	$students = Students::find($id);
    	$students->studentName 			= $request->studentName;
    	$students->fatherName 			= $request->fatherName;
    	$students->email 				= $request->email;
    	$students->fatherOccupation 	= $request->fatherOccupation;
    	$students->address 				= $request->address;
    	$students->fatherContactNo		= $request->fatherContactNo;
    	$students->nameOfInstitution	= $request->nameOfInstitution;
    	$students->fatherNIDno 			= $request->fatherNIDno;
    	$students->subject 				= $request->subject;
    	$students->motherName 			= $request->motherName;
    	$students->passingYear			= $request->passingYear;
    	$students->motherOccupation 	= $request->motherOccupation;
    	$students->dateOfBirth 			= $request->dateOfBirth;
    	$students->motherContactNo 		= $request->motherContactNo;
    	$students->religion 			= $request->religion;
    	$students->motherNIDno 			= $request->motherNIDno;
    	$students->save();
    	return redirect(route('home'))->with('successMsg', 'Student Updated Successfully!!!');
    }

    public function delete($id)
    {
    	Students::find($id)->delete();
    	return redirect(route('home'))->with('successMsg', 'Student Deleted Successfully!!!');
    }

}
