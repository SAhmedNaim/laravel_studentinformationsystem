<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StudentController@showStudents')->name('home');

Route::get('insert', 'StudentController@insert')->name('insert');
Route::post('insert', 'StudentController@insertStudents')->name('insertStudents');

Route::get('update/{id}', 'StudentController@update')->name('update');
Route::post('updateStudents/{id}', 'StudentController@updateStudents')->name('updateStudents');

Route::delete('delete/{id}', 'StudentController@delete')->name('delete');
